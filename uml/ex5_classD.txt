@startuml
class citizen{
-name
-ID
-makeSugg()
-registIncid()
}

interface city{
-cityname
}

class council{
- solveSugg()
- solveIncid()
}


class m_suggestion{
-ID

}

class r_incidence{
-ID

}

class solve_sugg{
-street_name
-type
-date
-hour
+workerID
+teamWork
}

class solve_incd{
-street_name
-type
-date
-hour
+workerID
+teamWork
}

class mng_incd{
-street_name
-type
-date
-hour
-getLocation()
-getDate()
-getH()
}

class mng_sugg{
-street_name
-type
-date
-hour
-getLocation()
-getDate()
-getH()
}


council -up-> city


citizen "1" *--> "0..*" r_incidence
citizen "1" *--> "0..*" m_suggestion
council "1" *--> "0..*" solve_sugg
council "1" *--> "0..*" solve_incd

m_suggestion "1" o--> "1" mng_sugg
r_incidence "1" o--> "1" mng_incd
solve_sugg *--|> mng_sugg
solve_incd *--|> mng_incd
@enduml
